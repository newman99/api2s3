# AWS API Gateway to S3

Upload a file to S3 using API Gateway.

This project is built using the following tools and libraries:
* [AWS](https://aws.amazon.com/) - Amazon Web Services
  * [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) - manage your AWS services from the command line
  * [CloudFormation](https://aws.amazon.com/cloudformation/) - model and provision all your cloud infrastructure resources
  * [API Gateway](https://aws.amazon.com/api-gateway/) - deliver robust, secure, and scalable mobile and web application backends
  * [S3](https://aws.amazon.com/s3/) - object storage built to store and retrieve any amount of data from anywhere
* [troposphere](https://github.com/cloudtools/troposphere) - Python library to create AWS CloudFormation descriptions
* [Git](https://git-scm.com/) - version control (optional)

## Getting Started


### Prerequisites

This project has only been tested with [Python 3](https://www.python.org/download/releases/3.0/).

An [AWS](https://aws.amazon.com/) account is required.
Install and configure [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html).

Install troposphere using pip:

```bash
pip install troposphere
```

Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (optional).

## Deployment

Create the CloudFormation template:

```bash
python3 make_template.py > template.json
aws cloudformation create-stack --stack-name=as1 --template-body file://template.json --capabilities CAPABILITY_IAM --profile=<aws_profile>
```

## Test by uploading an SVG file

```bash
curl -X PUT \
  https://<api-gateway-id>c.execute-api.<region>.amazonaws.com/<stage>/<bucket-name>/<filename.svg> \
  -H 'Content-Type: application/svg+xml' \
  --upload-file <filename.svg>
```

## Authors

* **Matthew Newman**

## License

This project is licensed under The Unlicense - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Create a REST API as an Amazon S3 Proxy in API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/integrating-api-with-aws-services-s3.html)
* [ApiGateway using Troposphere](https://github.com/cloudtools/troposphere/blob/master/examples/ApiGateway.py)
