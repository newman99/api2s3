"""Create an API Gateway and S3 Bucket."""
from troposphere import Ref, Template, Output
from troposphere.apigateway import RestApi, Method
from troposphere.apigateway import Resource, MethodResponse
from troposphere.apigateway import Integration, IntegrationResponse
from troposphere.apigateway import Deployment, Stage, ApiStage
from troposphere.apigateway import UsagePlan, QuotaSettings, ThrottleSettings
from troposphere.apigateway import ApiKey, StageKey, UsagePlanKey
from troposphere.iam import Role, Policy
from troposphere.s3 import Bucket, PublicRead
from troposphere import GetAtt, Join


def create(region='us-east-1', stage_name='dev'):
    """Main."""
    t = Template('API Gateway to S3')

    # Create the Api Gateway
    rest_api = t.add_resource(RestApi(
        "API2S3Example",
        Name="API2S3Example"
    ))

    # Create a role for the S3 bucket
    t.add_resource(Role(
        "API2S3ExampleRole",
        Path="/",
        Policies=[Policy(
            PolicyName="Put2S3",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [{
                    "Action": "s3:*",
                    "Resource": "*",
                    "Effect": "Allow"
                }]
            })],
        AssumeRolePolicyDocument={
            "Version": "2012-10-17",
            "Statement": [{
                "Action": ["sts:AssumeRole"],
                "Effect": "Allow",
                "Principal": {
                    "Service": [
                        "apigateway.amazonaws.com"
                    ]
                }
            }]
        },
    ))

    # Create an S3 bucket
    s3bucket = t.add_resource(Bucket(
        "API2S3ExampleBucket", AccessControl=PublicRead,)
        )

    # Add S3 bucket name to output
    t.add_output(Output(
        "API2S3ExampleBucket",
        Value=Ref(s3bucket),
        Description="API Gateway to S3 Bucket"
    ))

    # Add bucket resource
    resourceBucket = t.add_resource(Resource(
        "API2S3ExampleResourceBucket",
        RestApiId=Ref(rest_api),
        PathPart="{folder}",
        ParentId=GetAtt("API2S3Example", "RootResourceId"),
    ))

    # Add file resource
    resourceItem = t.add_resource(Resource(
        "API2S3ExampleResourceItem",
        RestApiId=Ref(rest_api),
        PathPart="{item}",
        ParentId=Ref(resourceBucket),
    ))

    # Add PUT method for file
    t.add_resource(Method(
        "API2S3ExampleMethodItem",
        DependsOn=['API2S3ExampleResourceItem'],
        ApiKeyRequired=True,
        RestApiId=Ref(rest_api),
        AuthorizationType="NONE",
        ResourceId=Ref(resourceItem),
        HttpMethod="PUT",
        MethodResponses=[
            MethodResponse(
                "CatResponse",
                StatusCode='200'
            )
        ],
        RequestParameters={
            "method.request.path.folder": True,
            "method.request.path.item": True
        },
        Integration=Integration(
            Credentials=GetAtt("API2S3ExampleRole", "Arn"),
            Type="AWS",
            IntegrationHttpMethod='PUT',
            IntegrationResponses=[
                IntegrationResponse(
                    StatusCode='200'
                )
            ],
            Uri="arn:aws:apigateway:{}:s3:path/{{bucket}}/{{object}}".
                format(region),
            RequestParameters={
                "integration.request.path.bucket":
                    'method.request.path.folder',
                "integration.request.path.object":
                    'method.request.path.item'
            }
        ),
    ))

    # Create a deployment
    deployment = t.add_resource(Deployment(
        "%sDeployment" % stage_name,
        RestApiId=Ref(rest_api),
        DependsOn=['API2S3ExampleMethodItem']
    ))

    # Create deployment stage
    stage = t.add_resource(Stage(
        '%sStage' % stage_name,
        StageName=stage_name,
        RestApiId=Ref(rest_api),
        DeploymentId=Ref(deployment)
    ))

    # Create API key
    key = t.add_resource(ApiKey(
        "ApiKey",
        StageKeys=[StageKey(
            RestApiId=Ref(rest_api),
            StageName=Ref(stage)
        )]
    ))

    # Create an API usage plan
    usagePlan = t.add_resource(UsagePlan(
        "ExampleUsagePlan",
        UsagePlanName="ExampleUsagePlan",
        Description="Example usage plan",
        Quota=QuotaSettings(
            Limit=50000,
            Period="MONTH"
        ),
        Throttle=ThrottleSettings(
            BurstLimit=500,
            RateLimit=5000
        ),
        ApiStages=[
            ApiStage(
                ApiId=Ref(rest_api),
                Stage=Ref(stage)
            )]
    ))

    # Tie the usage plan and key together
    t.add_resource(UsagePlanKey(
        "ExampleUsagePlanKey",
        KeyId=Ref(key),
        KeyType="API_KEY",
        UsagePlanId=Ref(usagePlan)
    ))

    # Add the deployment endpoint as an output
    t.add_output([
        Output(
            "ApiEndpoint",
            Value=Join("", [
                "https://",
                Ref(rest_api),
                ".execute-api.us-east-1.amazonaws.com/",
                stage_name
            ]),
            Description="Endpoint for this stage of the API"
        ),
        Output(
            "ApiGateway",
            Value=Ref(rest_api),
            Description="API Gateway ID"
        ),
        Output(
            "ApiKey",
            Value=Ref(key),
            Description="API key"
        ),
    ])

    return t.to_json()


if __name__ == '__main__':
    json = create()
    print(json)
